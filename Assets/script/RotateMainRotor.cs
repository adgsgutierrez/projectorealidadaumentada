﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateMainRotor : MonoBehaviour
{

    public ParticleSystem humo;
    // Start is called before the first frame update
    void Start()
    {
        humo.Play();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0 , 0 , 1857) * Time.deltaTime);
        
    }
}
